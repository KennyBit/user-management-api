<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'idnumber', 'phone', 'email','password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function getValidationRule () {
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }

    public static function getRegisterValidationRule () {
        return [ 
            'firstname' => 'required',
            'lastname' => 'required',
            'idnumber' => 'required',
            'phone' => 'required', 
            'email' => 'required|email', 
            'password' => 'required|confirmed', 
            'password_confirmation' => 'required',   
        ];
    }

    public function requisition()
    {
        return $this->hasMany('App\Requisition', 'user');
    }

    public function trip()
    {
        return $this->hasMany('App\Trip', 'user');
    }
}
