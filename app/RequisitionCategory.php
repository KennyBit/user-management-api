<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequisitionCategory extends Model
{

     public function requisition_item()
    {
        return $this->hasMany('App\RequisitionItem', 'requisition_category');
    }

}
