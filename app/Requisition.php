<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Requisition extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'date',
        'trip',
        'user',
        'status'
    ];

    protected $dates = ['deleted_at'];

    // public function requisition_category()
    // {
    //     return $this->belongsTo('App\RequisitionCategory', 'requisition_category');
    // }

    public function user()
    {
        return $this->belongsTo('App\User', 'user');
    }

    public function trip()
    {
        return $this->belongsTo('App\Trip', 'trip');
    }

    public function requisition_item()
    {
        return $this->hasMany('App\RequisitionItem', 'requisition');
    }

    public static function getValidationRule () {
        return [
            'name' => 'required',
            'description' => 'required',
            'date' => 'required',
            
        ];
    }

      public static function getEditValidationRule () {
        return [
            'name' => 'required',
            'description' => 'required',
            'date' => 'required',
            
        ];
    }

   
}
