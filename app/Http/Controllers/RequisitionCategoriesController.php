<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

use App\RequisitionCategory;

class RequisitionCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
        {
            $this->middleware('auth');
        }

    public function index()
    {
        return RequisitionCategory::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $requisitioncategory = RequisitionCategory::find($id);

        if($requisitioncategory) {
            return response()->json(['status'=>'success',
            'message'=>'requisitioncategory_found','data'=>$requisitioncategory],Response::HTTP_OK);
        }

        return response()->json(['status'=>'error', 'message'=>'requisitioncategory_not_found'],Response::HTTP_CREATED);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequisitionCategory  $requisitionCategory
     * @return \Illuminate\Http\Response
     */
    public function show(RequisitionCategory $requisitionCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequisitionCategory  $requisitionCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(RequisitionCategory $requisitionCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequisitionCategory  $requisitionCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequisitionCategory $requisitionCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequisitionCategory  $requisitionCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequisitionCategory $requisitionCategory)
    {
        //
    }
}
