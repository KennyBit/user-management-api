<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;

use App\Requisition;
use App\Trip;
use App\User;

class RequisitionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
        {
            $this->middleware('auth');
        }

    public function index()
    {
        return Requisition::with('trip', 'user')->orderBy('id','desc')->get();
        // $items = Item::orderBy('created_at','asc')->take(10)->get();
    }

    public function view($id){
        $requisition = Requisition::with('trip', 'user')->find($id);
        $requisition_items = DB::table('requisition_items')->where('requisition', $id)->get();

        $data = [
            'requisition' => $requisition,
            'requisition_items' => $requisition_items
        ];

        
        if($requisition){

            $response = $json_array;
            return json_encode($response);
            // return response()->json(['status'=>'success', 'message'=>'requisition_found','data'=>$data],Response::HTTP_OK);
        }

         $json_array = array(
                'success' => 2,
                'message' => 'no requition items',
            );

            $response = $json_array;
            return json_encode($response);
        // return response()->json(['status'=>'error', 'message'=>'requisition_not_found'],Response::HTTP_CREATED);
    }


    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(),
        Requisition::getValidationRule());
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
            Response::HTTP_CREATED);
        }

        
        $requisition = new Requisition;
        // $response = Requisition::create($requisition);
        // $requisition = $request->all();
        $requisition->name = $request->input('name');
        $requisition->description = $request->input('description');
        $requisition->date = $request->input('date');
        $requisition->user = auth()->user()->id;
        $requisition->status = $request->input('status');
        $requisition->status = 0;
        $requisition->trip = $request->input('trip');
        $requisition->save();

        if($requisition)
         
            return response()->json(['status'=>'success', 'message'=>'requisition created','data'=>$requisition],Response::HTTP_OK);
            return response()->json(['status'=>'error', 'message'=>'requisition creation failed'],Respponse::HTTP_CREATED);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function show(Requisition $requisition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function edit(Requisition $requisition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
   public function submit(Request $request, $id)
    {
        $requisition = Requisition::find($id);
        $requisition->status = $request->input('status');
        $requisition->status = 1;        
        $requisition->save();
    
        
        return response()->json(['status'=>'success', 'message'=>'Requisition Submitted','data'=>$requisition],Response::HTTP_OK);
       
    }

    public function approve(Request $request, $id)
    {
        $requisition = Requisition::find($id);
        $requisition->status = $request->input('status');
        $requisition->status = 2;        
        $requisition->save();
    
        
        return response()->json(['status'=>'success', 'message'=>'Requisition Approved','data'=>$requisition],Response::HTTP_OK);
       
    }

    public function decline(Request $request, $id)
    {
        $requisition = Requisition::find($id);
        $requisition->status = $request->input('status');
        $requisition->status = 3;        
        $requisition->save();
    
        
        return response()->json(['status'=>'success', 'message'=>'Requisition Declined','data'=>$requisition],Response::HTTP_OK);
       
    }

      public function given(Request $request, $id)
    {
        $requisition = Requisition::find($id);
        $requisition->status = $request->input('status');
        $requisition->status = 4;        
        $requisition->save();
    
        
        return response()->json(['status'=>'success', 'message'=>'Requisition Given','data'=>$requisition],Response::HTTP_OK);
       
    }

    public function update(Request $request, $id)
    {
        $requisition = Requisition::find($id);

        $validator = Validator::make($request->all(),
        Requisition::getEditValidationRule());
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
            Response::HTTP_CREATED);
        }

        if($requisition){
            if ($request->has('name'))
                {
                    $requisition->name = $request->input('name');
                }
            if ($request->has('description'))
                {
                    $requisition->description = $request->input('description');
                }    
            if ($request->has('date'))
                {
                    $requisition->date = $request->input('date');
                }
                if ($request->has('trip'))
			    {
			    	$requisition->trip = $request->input('trip');
			    }
            $requisition->save();
            
            return response()->json(['status'=>'success', 'message'=>'requisition updated','data'=>$requisition],Response::HTTP_OK);
        }

        return response()->json(['status'=>'error', 'message'=>'requisition_not_found'],Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $requisition = Requisition::find($id);

        if($requisition){
        	$requisition->delete();

        	return response()->json(['status'=>'success', 'message'=>'requisition_deleted'],Response::HTTP_OK);
    	}

        return response()->json(['status'=>'error', 'message'=>'requisition_not_found'],Response::HTTP_CREATED);
    }
}
