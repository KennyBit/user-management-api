<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

use App\Trip;
use App\Route;
use App\Requisition;

class TripsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Trip::with('client', 'user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {   
        $trip = Requisition::with('client', 'user')->find($id);
        
        if($trip){
            return response()->json(['status'=>'success', 'message'=>'trip found','data'=>$trip],Response::HTTP_OK);
        }

        return response()->json(['status'=>'error', 'message'=>'trip not found'],Response::HTTP_CREATED);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)
    {
       

        $validator = Validator::make($request->all(),
        Trip::getValidationRule());
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
            Response::HTTP_CREATED);
        }        
        $trip = new Trip;
        $trip->name = $request->input('name');
        $trip->start_date = $request->input('start_date');
        $trip->end_date = $request->input('end_date');
        $trip->user = auth()->user()->id;
        $trip->client = $request->input('client');
        $trip->status = $request->input('status');
        $trip->status = 0;
        $trip->save();

        if($trip)
            return response()->json(['status'=>'success', 'message'=>'Trip created','data'=>$trip],Response::HTTP_OK);
            return response()->json(['status'=>'error', 'message'=>'Trip creation failed'],Respponse::HTTP_CREATED);
    }


    public function update(Request $request, $id)
    {
        $trip = Trip::find($id);

    	$validator = Validator::make($request->all(), Trip::getEditValidationRule());
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], Response::HTTP_CREATED);            
        }

    	if($trip){

    		if ($request->has('name'))
			    {
			    	$trip->name = $request->input('name');
                }
                
                if ($request->has('start_date'))
			    {
			    	$trip->from_when = $request->input('start_date');
                }
                
                if ($request->has('end_date'))
			    {
			    	$trip->to_when = $request->input('end_date');
                }
                if ($request->has('client'))
			    {
			    	$trip->client = $request->input('client');
			    }

			$trip->save();

    		return response()->json(['status'=>'success', 'message'=>'trip_updated','data'=>$trip],Response::HTTP_OK);
    	}

    	return response()->json(['status'=>'error', 'message'=>'trip_not_found'],Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function edit(Trip $trip)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $trip = Trip::find($id);

        if($trip){
        	$trip->delete();

        	return response()->json(['status'=>'success', 'message'=>'trip_deleted'],Response::HTTP_OK);
    	}

        return response()->json(['status'=>'error', 'message'=>'trip_not_found'],Response::HTTP_CREATED);
    }
}
