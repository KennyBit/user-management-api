<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => 'Ken',
            'lastname' => 'Muriithi',
            'idnumber' => '34894344',
            'phone' => 'phone',
            'email' => 'test@test.com',
            'password' => app('hash')->make('password'),
        ],
        [
            'firstname' => 'john',
            'lastname' => 'Doe',
            'idnumber' => '3489444',
            'phone' => 'phone',
            'email' => 'john@test.com',
            'password' => app('hash')->make('password'),
        ]);
    }
}
