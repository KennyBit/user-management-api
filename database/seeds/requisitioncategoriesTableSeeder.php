<?php

use Illuminate\Database\Seeder;

class RequisitionCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('requisition_categories')->insert([
            'name' => 'Transport'
        ]);
    }
}
